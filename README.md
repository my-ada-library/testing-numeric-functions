# What is this?

This is a generic package that can be useful to test numeric functions.

The idea of testing a numeric function is to "sample" it by calling it for several values of the input parameters and comparing the results with some "_ground truth results_."  The procedures in the package returns the maximum error found (in other words, the infinity-norm of the difference between the implemented function and the ground truth)

# How do I install it?

Just put the two files where the compiler will find them

# How do I use it?

## Package instantiation

The package is generic and it is parameterized by three types
* The type of the argument of the function to be tested (`Argument_Type`)
* The type of the value returned by the function (`Result_Type`)
* The type of the "Error," that is the difference between the expected value and the returned one (`Error_Type`)

Those types can be quite general, they do not need necessarily to be floating numbers.  The only constraints is that 
* The distance between two values is an _error_, the distance is computed by the function provided by the user 
```ada 
   with function Distance (X, Y : Result_Type) return Error_Type;
```
* The errors can be compared with the user-provided function
```ada
   with function ">" (X, Y : Error_Type) return Boolean is <>;
```

## Resources provided

1. A record type `Sample_Type` that represents a single "sample point" that contains an input argument and the corresponding _ground truth_ value
```ada
type Sample_Type is
      record
         X : Argument_Type;
         Y : Result_Type;
      end record;
```
2. An array of `Sample_Type` (`Sample_Array`) that collects the points used to test the function
3. A `Test_Function` procedure defined as
```
procedure Test_Function
     (Data    : Sample_Array;
      Tested  : access function (X : Argument_Type) return Result_Type;
      Max_Err : out Error_Type;
      Max_Pos : out Argument_Type;
      Logger  : Abstract_Logger'Class := Void_Logger);
```
where
* `Data` is the vector of the sample points used to test the function
* `Tested` is the access to the function to be tested
* `Max_Err` is the maximum error found, that is, the infinity-norm of the difference between the expected values and the computed ones
* `Where` is the argument corresponding to the error `Max_Err`
* `Logger` can be used to produce a verbose output.  The definition of the `Abstract_Logger` is
```ada
type Abstract_Logger is limited interface;

   procedure Log (Logger   : Abstract_Logger;
                  Argument : Argument_Type;
                  Result   : Result_Type;
                  Expected : Result_Type;
                  Error    : Error_Type)
   is abstract;
```
In other words, a _logger_ exports a procedure `Log` that accepts the current argument, the result obtained, the expected value and the corresponding error and... does whatever it wants with them (since it is a _logger_ one can expect it will print them, but this is not necessary).  The default value `Void_Logger` is just the `/dev/null` of the loggers: it does nothing.

## Example: testing a function with two arguments

Since the type of the input parameters and returned values are general, one can test a two-parameter function
```ada
function Foo(x, y: float) return float;
```
as follows.
1. First, instantiate the package
```ada
function distance(x, y: float) return Float
is (abs (x-y));

type Float_pair is array (1..2) of Float;

package Pair_Test is
     new Generic_Function_Test (Argument_Type  => Float_pair,
                                Result_Type    => float,
                                Error_Type     => float,
                                Smallest_Error => 0.0,
                                Distance       => Distance);
```
2. Then define a "gateway" function that accepts a `Float_Pair` and calls `Foo`
```ada
function Fun(x : float_pair) return Float
is (Foo(x(1), x(2)));
```
3. Now you can test the function with
```ada
 Test_Function (Data   => Test_Samples,
               Tested  => Fun'Access,
               Max_Err => Max_Err,
               Where   => Where);
 '''
